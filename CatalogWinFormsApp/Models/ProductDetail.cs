namespace CatalogWinFormsApp.Models
{
    public partial class ProductDetail
    {
        [System.ComponentModel.DisplayName("ID")]
        public int ProductId { get; set; }
        [System.ComponentModel.DisplayName("Product")]
        public string ProductName { get; set; } = "";
        [System.ComponentModel.DisplayName("Description")]
        public string ProductDescription { get; set; } = "";
        [System.ComponentModel.DisplayName("Product Image URL")]
        public string ProductImage { get; set; } = "";
    }
}