namespace CatalogWinFormsApp.Models
{
    public partial class Supplier
    {
        public int SupplierId { get; set; }
        public string SupplierName { get; set; } = "";
        public string SupplierContact { get; set; } = "";  
    }
}