﻿using Dapper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using CatalogWinFormsApp.Daos;
using DevExpress.XtraGrid.Views.Grid;
using Microsoft.Extensions.Configuration;
using CatalogWinFormsApp.Models;

namespace CatalogWinFormsApp
{
    public partial class MainForm : DevExpress.XtraEditors.XtraForm
    {
        BindingSource productBindingSource = new BindingSource();
        BindingSource paramsBindingSource = new BindingSource();
        IConfiguration config;
        public MainForm()
        {
            InitializeComponent();
            InitializeLayout();
            this.ActiveControl = welcomeHeaderLabel;
        }

        private void InitializeLayout()
        {
            catalogueSplitContainerControl.Visible = false;
            mainRibbonControl.Visible = true;
            connectionToDbLabel.Visible = true;
            connectionStatusPictureBox.Image = Image.FromFile(@"Images\red-circle.png");
        }

        private void LoadProductData()
        {
            ProductStatusDAO productStatusesDAO = new ProductStatusDAO(config);

            productBindingSource.DataSource = productStatusesDAO.GetAllProductsStatus();

            productStatsGridControl.DataSource = productBindingSource;

            productStatsGridView.FocusedRowHandle = 0;

            productStatsGridView.SelectRow(0);

            SelectProduct(0);
        }


        private void ShowCatalogueButton_Click(object sender, EventArgs e)
        {
            mainRibbonControl.Visible = true;
            catalogueSplitContainerControl.Visible = true;
            welcomeTableLayoutPanel.Visible = false;

            LoadProductData();
        }

        private void ReloadDataButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadProductData();
        }

        private void TestConnection()
        {
            config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            IDbConnection dbConnection = new SqlConnection(config.GetConnectionString("DefaultConnection"));

            try
            {
                DateTime outputTime = dbConnection.QuerySingle<DateTime>("SELECT GETDATE();");
                showCatalogueButton.Enabled = true;
                connectionStatusPictureBox.Image = Image.FromFile(@"Images\green-circle.png");
            }
            catch (Exception ex)
            {
                showCatalogueButton.Enabled = false;
                connectionStatusPictureBox.Image = Image.FromFile(@"Images\red-circle.png");
                MessageBox.Show("Error: " + ex.Message, "Error");
            }
        }

        private void SelectProduct(int rowClicked)
        {
            if (productStatsGridView.RowCount > rowClicked)
            {
                int productId = Convert.ToInt32(productStatsGridView.GetRowCellValue(rowClicked, "ProductId"));

                ProductDetailDAO productDetailDAO = new ProductDetailDAO(config);

                ProductDetail productDetails = productDetailDAO.GetProductDetails(productId);

                productPictureBox.Image = Image.FromFile(@"Images\" + productDetails.ProductImage);
                productHeaderLabel.Text = productDetails.ProductName;
                productDescriptionLabel.Text = productDetails.ProductDescription;

                ProductParameterDAO productParameterDAO = new ProductParameterDAO(config);

                paramsBindingSource.DataSource = productParameterDAO.GetProductParameters(productId);

                productParamsGridControl.DataSource = paramsBindingSource;
            }
        }

        private void ProductStatsGridControl_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (productStatsGridView.FocusedRowHandle < productStatsGridView.RowCount - 1 && e.KeyValue == 40)
            {
                SelectProduct(productStatsGridView.FocusedRowHandle + 1);
            }
            else if (productStatsGridView.FocusedRowHandle > 0 && e.KeyValue == 38)
            {
                SelectProduct(productStatsGridView.FocusedRowHandle - 1);
            }

        }

        private void ProductStatsGridView_RowClick(object sender, RowClickEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left && e.Clicks == 1)
            {
                if (e.RowHandle >= 0 && e.RowHandle < productStatsGridView.RowCount)
                {
                    SelectProduct(e.RowHandle);
                }
            }

        }

        private void ConfigureDatabaseButton_Click(object sender, EventArgs e)
        {
            Form configForm = new ConfigureForm(this.TestConnection);
            configForm.Show();
        }

        private void ConfigureDBbarButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form configForm = new ConfigureForm(this.TestConnection);
            configForm.Show();
        }

        private void MainForm_Shown_1(object sender, EventArgs e)
        {
            TestConnection();
        }
    }
}
