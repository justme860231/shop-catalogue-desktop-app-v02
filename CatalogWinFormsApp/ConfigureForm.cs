﻿using System;
using System.Windows.Forms;
using Microsoft.Extensions.Configuration;
using CatalogWinFormsApp.DB;
using CatalogWinFormsApp.Utils;

namespace CatalogWinFormsApp
{
    public partial class ConfigureForm : DevExpress.XtraEditors.XtraForm
    {
        Action _testConnection;
        IConfiguration config;

        public ConfigureForm(Action TestConnection)
        {
            InitializeComponent();
            //InitializeConfigFile();
            _testConnection = TestConnection;
            this.ActiveControl = configurationDBLabel;
        }

        private void InitializeConfigFile()
        {
            config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();
        }

        private void ConfigureDatabaseButton_Click(object sender, EventArgs e)
        {
            string message = "";

            bool isError = false;

            if (serverNameTextEdit.Text.Length > 0)
            {
                ServerChanger serverChanger = new ServerChanger("appsettings.json");
                
                bool isValid = serverChanger.TestServerName(serverNameTextEdit.Text);
                
                isError = !isValid;
                
                if (isValid)
                {
                    serverChanger.SetServerName(serverNameTextEdit.Text);
                    message += "Server Name changed.";
                }
            }

            if (serverNameTextEdit.Text.Length != 0 || createDatabaseCheckEdit.Checked || createSchemaCheckEdit.Checked || insertDataCheckEdit.Checked)
            {
                InitializeConfigFile();
            }

            DBInitializer databaseInit = new DBInitializer(config);

            if (createDatabaseCheckEdit.Checked && !isError)
            {
                if (databaseInit.CreateDatabase())
                {
                    message += "Database successfuly created. ";
                }
                else
                {
                    isError = true;
                }
            }
            if (createSchemaCheckEdit.Checked && !isError)
            {
                if (databaseInit.CreateSchema())
                {
                    message += "Schema successfuly created. ";
                }
                else
                {
                    isError = true;
                }
            }
            if (insertDataCheckEdit.Checked && !isError)
            {
                if (databaseInit.InsertInToTables())
                {
                    message += "All data inserted in to the tables.";
                }
                else
                {
                    isError = true;
                }
            }

            if (message.Length == 0)
            {
                message = "No changes were made.";
            }

            if (!isError)
            {
                if (MessageBox.Show(message, "Confirmation", MessageBoxButtons.OK) == DialogResult.OK)
                {
                    if (message != "No changes were made.")
                    {
                        _testConnection();
                    }

                    this.Close();
                }
            }
        }

        private void CreateDatabaseCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            createSchemaCheckEdit.Checked = true;
            insertDataCheckEdit.Checked = true;
        }

        private void CreateSchemaCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            insertDataCheckEdit.Checked = true;
        }
    }
}