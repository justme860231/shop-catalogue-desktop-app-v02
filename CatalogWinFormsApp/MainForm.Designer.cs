﻿namespace CatalogWinFormsApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            this.connectionToDbLabel = new System.Windows.Forms.Label();
            this.connectionStatusPictureBox = new System.Windows.Forms.PictureBox();
            this.Home = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.reloadDataButton = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.configureDBbarButton = new DevExpress.XtraBars.BarButtonItem();
            this.mainRibbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.productPictureBox = new System.Windows.Forms.PictureBox();
            this.productDetailGroupBox = new System.Windows.Forms.GroupBox();
            this.productDescriptionLabel = new System.Windows.Forms.Label();
            this.productHeaderLabel = new DevExpress.XtraEditors.LabelControl();
            this.productParamsGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.productStatsGridControl = new DevExpress.XtraGrid.GridControl();
            this.productStatsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.catalogueSplitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this.tablePanel1 = new DevExpress.Utils.Layout.TablePanel();
            this.showCatalogueButton = new DevExpress.XtraEditors.SimpleButton();
            this.configureDatabaseButton = new DevExpress.XtraEditors.SimpleButton();
            this.welcomeHeaderLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.welcomeTextLabel = new System.Windows.Forms.Label();
            this.welcomeTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.connectionStatusPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainRibbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productPictureBox)).BeginInit();
            this.productDetailGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productParamsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productStatsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productStatsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.catalogueSplitContainerControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.catalogueSplitContainerControl.Panel1)).BeginInit();
            this.catalogueSplitContainerControl.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.catalogueSplitContainerControl.Panel2)).BeginInit();
            this.catalogueSplitContainerControl.Panel2.SuspendLayout();
            this.catalogueSplitContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tablePanel1)).BeginInit();
            this.tablePanel1.SuspendLayout();
            this.welcomeTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // connectionToDbLabel
            // 
            this.connectionToDbLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.connectionToDbLabel.AutoSize = true;
            this.connectionToDbLabel.Location = new System.Drawing.Point(41, 704);
            this.connectionToDbLabel.Name = "connectionToDbLabel";
            this.connectionToDbLabel.Size = new System.Drawing.Size(128, 13);
            this.connectionToDbLabel.TabIndex = 4;
            this.connectionToDbLabel.Text = "Connection to DB Status:";
            // 
            // connectionStatusPictureBox
            // 
            this.connectionStatusPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.connectionStatusPictureBox.Location = new System.Drawing.Point(169, 704);
            this.connectionStatusPictureBox.Name = "connectionStatusPictureBox";
            this.connectionStatusPictureBox.Size = new System.Drawing.Size(15, 15);
            this.connectionStatusPictureBox.TabIndex = 5;
            this.connectionStatusPictureBox.TabStop = false;
            // 
            // Home
            // 
            this.Home.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2});
            this.Home.Name = "Home";
            this.Home.Text = "Home";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.reloadDataButton);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Data";
            // 
            // reloadDataButton
            // 
            this.reloadDataButton.Caption = "Reload";
            this.reloadDataButton.Id = 1;
            this.reloadDataButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("reloadDataButton.ImageOptions.Image")));
            this.reloadDataButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("reloadDataButton.ImageOptions.LargeImage")));
            this.reloadDataButton.Name = "reloadDataButton";
            this.reloadDataButton.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.reloadDataButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ReloadDataButton_ItemClick);
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.configureDBbarButton);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Database";
            // 
            // configureDBbarButton
            // 
            this.configureDBbarButton.Caption = "Configure DB\r\n";
            this.configureDBbarButton.Id = 2;
            this.configureDBbarButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("configureDBbarButton.ImageOptions.Image")));
            this.configureDBbarButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("configureDBbarButton.ImageOptions.LargeImage")));
            this.configureDBbarButton.Name = "configureDBbarButton";
            this.configureDBbarButton.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.configureDBbarButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ConfigureDBbarButton_ItemClick);
            // 
            // mainRibbonControl
            // 
            this.mainRibbonControl.ExpandCollapseItem.Id = 0;
            this.mainRibbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.mainRibbonControl.ExpandCollapseItem,
            this.reloadDataButton,
            this.configureDBbarButton});
            this.mainRibbonControl.Location = new System.Drawing.Point(0, 0);
            this.mainRibbonControl.MaxItemId = 3;
            this.mainRibbonControl.Name = "mainRibbonControl";
            this.mainRibbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.Home});
            this.mainRibbonControl.Size = new System.Drawing.Size(1043, 150);
            // 
            // productPictureBox
            // 
            this.productPictureBox.Location = new System.Drawing.Point(7, 6);
            this.productPictureBox.Name = "productPictureBox";
            this.productPictureBox.Size = new System.Drawing.Size(230, 230);
            this.productPictureBox.TabIndex = 0;
            this.productPictureBox.TabStop = false;
            // 
            // productDetailGroupBox
            // 
            this.productDetailGroupBox.Controls.Add(this.productDescriptionLabel);
            this.productDetailGroupBox.Controls.Add(this.productHeaderLabel);
            this.productDetailGroupBox.Location = new System.Drawing.Point(281, -1);
            this.productDetailGroupBox.Name = "productDetailGroupBox";
            this.productDetailGroupBox.Size = new System.Drawing.Size(280, 237);
            this.productDetailGroupBox.TabIndex = 1;
            this.productDetailGroupBox.TabStop = false;
            this.productDetailGroupBox.Text = "Product Detail";
            // 
            // productDescriptionLabel
            // 
            this.productDescriptionLabel.AutoSize = true;
            this.productDescriptionLabel.Location = new System.Drawing.Point(15, 88);
            this.productDescriptionLabel.MaximumSize = new System.Drawing.Size(250, 600);
            this.productDescriptionLabel.Name = "productDescriptionLabel";
            this.productDescriptionLabel.Size = new System.Drawing.Size(65, 13);
            this.productDescriptionLabel.TabIndex = 1;
            this.productDescriptionLabel.Text = "Product info";
            // 
            // productHeaderLabel
            // 
            this.productHeaderLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.productHeaderLabel.Appearance.Options.UseFont = true;
            this.productHeaderLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.productHeaderLabel.Location = new System.Drawing.Point(13, 20);
            this.productHeaderLabel.MaximumSize = new System.Drawing.Size(250, 50);
            this.productHeaderLabel.Name = "productHeaderLabel";
            this.productHeaderLabel.Size = new System.Drawing.Size(250, 25);
            this.productHeaderLabel.TabIndex = 0;
            this.productHeaderLabel.Text = "Product";
            // 
            // productParamsGridControl
            // 
            this.productParamsGridControl.Location = new System.Drawing.Point(600, 6);
            this.productParamsGridControl.MainView = this.gridView2;
            this.productParamsGridControl.Name = "productParamsGridControl";
            this.productParamsGridControl.Size = new System.Drawing.Size(407, 230);
            this.productParamsGridControl.TabIndex = 2;
            this.productParamsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2,
            this.gridView1});
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.productParamsGridControl;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.Editable = false;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.productParamsGridControl;
            this.gridView1.Name = "gridView1";
            // 
            // productStatsGridControl
            // 
            this.productStatsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.RelationName = "Level1";
            this.productStatsGridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.productStatsGridControl.Location = new System.Drawing.Point(0, 0);
            this.productStatsGridControl.MainView = this.productStatsGridView;
            this.productStatsGridControl.Name = "productStatsGridControl";
            this.productStatsGridControl.Size = new System.Drawing.Size(1043, 440);
            this.productStatsGridControl.TabIndex = 0;
            this.productStatsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.productStatsGridView,
            this.gridView3});
            this.productStatsGridControl.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.ProductStatsGridControl_PreviewKeyDown);
            // 
            // productStatsGridView
            // 
            this.productStatsGridView.GridControl = this.productStatsGridControl;
            this.productStatsGridView.Name = "productStatsGridView";
            this.productStatsGridView.OptionsBehavior.Editable = false;
            this.productStatsGridView.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.ProductStatsGridView_RowClick);
            // 
            // gridView3
            // 
            this.gridView3.GridControl = this.productStatsGridControl;
            this.gridView3.Name = "gridView3";
            // 
            // catalogueSplitContainerControl
            // 
            this.catalogueSplitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.catalogueSplitContainerControl.Horizontal = false;
            this.catalogueSplitContainerControl.Location = new System.Drawing.Point(0, 150);
            this.catalogueSplitContainerControl.Name = "catalogueSplitContainerControl";
            // 
            // catalogueSplitContainerControl.Panel1
            // 
            this.catalogueSplitContainerControl.Panel1.Controls.Add(this.productStatsGridControl);
            this.catalogueSplitContainerControl.Panel1.Text = "Panel1";
            // 
            // catalogueSplitContainerControl.Panel2
            // 
            this.catalogueSplitContainerControl.Panel2.Controls.Add(this.productParamsGridControl);
            this.catalogueSplitContainerControl.Panel2.Controls.Add(this.productDetailGroupBox);
            this.catalogueSplitContainerControl.Panel2.Controls.Add(this.productPictureBox);
            this.catalogueSplitContainerControl.Panel2.Text = "Panel2";
            this.catalogueSplitContainerControl.Size = new System.Drawing.Size(1043, 572);
            this.catalogueSplitContainerControl.SplitterPosition = 440;
            this.catalogueSplitContainerControl.TabIndex = 1;
            // 
            // tablePanel1
            // 
            this.tablePanel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tablePanel1.AutoSize = true;
            this.tablePanel1.Columns.AddRange(new DevExpress.Utils.Layout.TablePanelColumn[] {
            new DevExpress.Utils.Layout.TablePanelColumn(DevExpress.Utils.Layout.TablePanelEntityStyle.AutoSize, 30.34F),
            new DevExpress.Utils.Layout.TablePanelColumn(DevExpress.Utils.Layout.TablePanelEntityStyle.AutoSize, 30.33F)});
            this.tablePanel1.Controls.Add(this.showCatalogueButton);
            this.tablePanel1.Controls.Add(this.configureDatabaseButton);
            this.tablePanel1.Location = new System.Drawing.Point(3, 489);
            this.tablePanel1.Name = "tablePanel1";
            this.tablePanel1.Rows.AddRange(new DevExpress.Utils.Layout.TablePanelRow[] {
            new DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.AutoSize, 26F)});
            this.tablePanel1.Size = new System.Drawing.Size(1036, 50);
            this.tablePanel1.TabIndex = 3;
            this.tablePanel1.UseSkinIndents = true;
            // 
            // showCatalogueButton
            // 
            this.tablePanel1.SetColumn(this.showCatalogueButton, 1);
            this.showCatalogueButton.Location = new System.Drawing.Point(718, 13);
            this.showCatalogueButton.Margin = new System.Windows.Forms.Padding(200, 3, 200, 3);
            this.showCatalogueButton.Name = "showCatalogueButton";
            this.tablePanel1.SetRow(this.showCatalogueButton, 0);
            this.showCatalogueButton.Size = new System.Drawing.Size(107, 23);
            this.showCatalogueButton.TabIndex = 7;
            this.showCatalogueButton.Text = "Catalogue";
            this.showCatalogueButton.Click += new System.EventHandler(this.ShowCatalogueButton_Click);
            // 
            // configureDatabaseButton
            // 
            this.tablePanel1.SetColumn(this.configureDatabaseButton, 0);
            this.configureDatabaseButton.Location = new System.Drawing.Point(211, 13);
            this.configureDatabaseButton.Margin = new System.Windows.Forms.Padding(200, 3, 200, 3);
            this.configureDatabaseButton.Name = "configureDatabaseButton";
            this.tablePanel1.SetRow(this.configureDatabaseButton, 0);
            this.configureDatabaseButton.Size = new System.Drawing.Size(107, 23);
            this.configureDatabaseButton.TabIndex = 6;
            this.configureDatabaseButton.Text = "Configure DB";
            this.configureDatabaseButton.Click += new System.EventHandler(this.ConfigureDatabaseButton_Click);
            // 
            // welcomeHeaderLabel
            // 
            this.welcomeHeaderLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.welcomeHeaderLabel.AutoSize = true;
            this.welcomeHeaderLabel.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.welcomeHeaderLabel.Location = new System.Drawing.Point(207, 68);
            this.welcomeHeaderLabel.Name = "welcomeHeaderLabel";
            this.welcomeHeaderLabel.Size = new System.Drawing.Size(628, 35);
            this.welcomeHeaderLabel.TabIndex = 0;
            this.welcomeHeaderLabel.Text = "Welcome to the Shop Catalogue Manager!";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(1046, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1, 35);
            this.label1.TabIndex = 1;
            this.label1.Text = "Welcome to the Shop Catalogue Manager!";
            // 
            // welcomeTextLabel
            // 
            this.welcomeTextLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.welcomeTextLabel.AutoSize = true;
            this.welcomeTextLabel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.welcomeTextLabel.Location = new System.Drawing.Point(278, 266);
            this.welcomeTextLabel.MaximumSize = new System.Drawing.Size(500, 400);
            this.welcomeTextLabel.Name = "welcomeTextLabel";
            this.welcomeTextLabel.Size = new System.Drawing.Size(487, 95);
            this.welcomeTextLabel.TabIndex = 2;
            this.welcomeTextLabel.Text = resources.GetString("welcomeTextLabel.Text");
            this.welcomeTextLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // welcomeTableLayoutPanel
            // 
            this.welcomeTableLayoutPanel.ColumnCount = 2;
            this.welcomeTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.welcomeTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 0F));
            this.welcomeTableLayoutPanel.Controls.Add(this.welcomeTextLabel, 0, 1);
            this.welcomeTableLayoutPanel.Controls.Add(this.label1, 1, 0);
            this.welcomeTableLayoutPanel.Controls.Add(this.welcomeHeaderLabel, 0, 0);
            this.welcomeTableLayoutPanel.Controls.Add(this.tablePanel1, 0, 2);
            this.welcomeTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.welcomeTableLayoutPanel.Location = new System.Drawing.Point(0, 150);
            this.welcomeTableLayoutPanel.Name = "welcomeTableLayoutPanel";
            this.welcomeTableLayoutPanel.RowCount = 3;
            this.welcomeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.welcomeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.welcomeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.welcomeTableLayoutPanel.Size = new System.Drawing.Size(1043, 572);
            this.welcomeTableLayoutPanel.TabIndex = 2;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1043, 722);
            this.Controls.Add(this.connectionStatusPictureBox);
            this.Controls.Add(this.connectionToDbLabel);
            this.Controls.Add(this.welcomeTableLayoutPanel);
            this.Controls.Add(this.catalogueSplitContainerControl);
            this.Controls.Add(this.mainRibbonControl);
            this.MinimumSize = new System.Drawing.Size(1038, 562);
            this.Name = "MainForm";
            this.Text = "Shop Catalogue";
            this.Shown += new System.EventHandler(this.MainForm_Shown_1);
            ((System.ComponentModel.ISupportInitialize)(this.connectionStatusPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainRibbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productPictureBox)).EndInit();
            this.productDetailGroupBox.ResumeLayout(false);
            this.productDetailGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productParamsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productStatsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productStatsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.catalogueSplitContainerControl.Panel1)).EndInit();
            this.catalogueSplitContainerControl.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.catalogueSplitContainerControl.Panel2)).EndInit();
            this.catalogueSplitContainerControl.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.catalogueSplitContainerControl)).EndInit();
            this.catalogueSplitContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tablePanel1)).EndInit();
            this.tablePanel1.ResumeLayout(false);
            this.welcomeTableLayoutPanel.ResumeLayout(false);
            this.welcomeTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label connectionToDbLabel;
        private System.Windows.Forms.PictureBox connectionStatusPictureBox;
        private DevExpress.XtraBars.Ribbon.RibbonPage Home;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem reloadDataButton;
        private DevExpress.XtraBars.Ribbon.RibbonControl mainRibbonControl;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem configureDBbarButton;
        private System.Windows.Forms.PictureBox productPictureBox;
        private System.Windows.Forms.GroupBox productDetailGroupBox;
        private System.Windows.Forms.Label productDescriptionLabel;
        private DevExpress.XtraEditors.LabelControl productHeaderLabel;
        private DevExpress.XtraGrid.GridControl productParamsGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl productStatsGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView productStatsGridView;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.SplitContainerControl catalogueSplitContainerControl;
        private DevExpress.Utils.Layout.TablePanel tablePanel1;
        private DevExpress.XtraEditors.SimpleButton showCatalogueButton;
        private DevExpress.XtraEditors.SimpleButton configureDatabaseButton;
        private System.Windows.Forms.Label welcomeHeaderLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label welcomeTextLabel;
        private System.Windows.Forms.TableLayoutPanel welcomeTableLayoutPanel;
    }
}

