﻿namespace CatalogWinFormsApp
{
    partial class ConfigureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.databaseConfigLabel = new System.Windows.Forms.Label();
            this.configurationDBLabel = new System.Windows.Forms.Label();
            this.boldOptionsDescriptionLabel1 = new System.Windows.Forms.Label();
            this.boldOptionsDescriptionLabel2 = new System.Windows.Forms.Label();
            this.boldOptionsDescriptionLabel3 = new System.Windows.Forms.Label();
            this.boldOptionsDescriptionLabel4 = new System.Windows.Forms.Label();
            this.optionsDescriptionLabel1 = new System.Windows.Forms.Label();
            this.optionsDescriptionLabel4 = new System.Windows.Forms.Label();
            this.optionsDescriptionLabel3 = new System.Windows.Forms.Label();
            this.optionsDescriptionLabel2 = new System.Windows.Forms.Label();
            this.serverNameLabel = new System.Windows.Forms.Label();
            this.serverNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.createDatabaseCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.createSchemaCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.insertDataCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.configureDatabaseButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.serverNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.createDatabaseCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.createSchemaCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.insertDataCheckEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // databaseConfigLabel
            // 
            this.databaseConfigLabel.AutoSize = true;
            this.databaseConfigLabel.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.databaseConfigLabel.Location = new System.Drawing.Point(97, 9);
            this.databaseConfigLabel.Name = "databaseConfigLabel";
            this.databaseConfigLabel.Size = new System.Drawing.Size(294, 29);
            this.databaseConfigLabel.TabIndex = 0;
            this.databaseConfigLabel.Text = "Database Configuration";
            // 
            // configurationDBLabel
            // 
            this.configurationDBLabel.AutoSize = true;
            this.configurationDBLabel.Location = new System.Drawing.Point(22, 58);
            this.configurationDBLabel.MaximumSize = new System.Drawing.Size(420, 400);
            this.configurationDBLabel.Name = "configurationDBLabel";
            this.configurationDBLabel.Size = new System.Drawing.Size(420, 26);
            this.configurationDBLabel.TabIndex = 1;
            this.configurationDBLabel.Text = "Welcome to the Database Configuration Wizard. Please make your selections below t" +
    "o tailor the setup according to your needs:";
            // 
            // boldOptionsDescriptionLabel1
            // 
            this.boldOptionsDescriptionLabel1.AutoSize = true;
            this.boldOptionsDescriptionLabel1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.boldOptionsDescriptionLabel1.Location = new System.Drawing.Point(22, 105);
            this.boldOptionsDescriptionLabel1.Name = "boldOptionsDescriptionLabel1";
            this.boldOptionsDescriptionLabel1.Size = new System.Drawing.Size(83, 13);
            this.boldOptionsDescriptionLabel1.TabIndex = 2;
            this.boldOptionsDescriptionLabel1.Text = "Server Name:";
            // 
            // boldOptionsDescriptionLabel2
            // 
            this.boldOptionsDescriptionLabel2.AutoSize = true;
            this.boldOptionsDescriptionLabel2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.boldOptionsDescriptionLabel2.Location = new System.Drawing.Point(22, 134);
            this.boldOptionsDescriptionLabel2.Name = "boldOptionsDescriptionLabel2";
            this.boldOptionsDescriptionLabel2.Size = new System.Drawing.Size(105, 13);
            this.boldOptionsDescriptionLabel2.TabIndex = 3;
            this.boldOptionsDescriptionLabel2.Text = "Create Database:";
            // 
            // boldOptionsDescriptionLabel3
            // 
            this.boldOptionsDescriptionLabel3.AutoSize = true;
            this.boldOptionsDescriptionLabel3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.boldOptionsDescriptionLabel3.Location = new System.Drawing.Point(22, 163);
            this.boldOptionsDescriptionLabel3.Name = "boldOptionsDescriptionLabel3";
            this.boldOptionsDescriptionLabel3.Size = new System.Drawing.Size(96, 13);
            this.boldOptionsDescriptionLabel3.TabIndex = 4;
            this.boldOptionsDescriptionLabel3.Text = "Create Schema:";
            // 
            // boldOptionsDescriptionLabel4
            // 
            this.boldOptionsDescriptionLabel4.AutoSize = true;
            this.boldOptionsDescriptionLabel4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.boldOptionsDescriptionLabel4.Location = new System.Drawing.Point(22, 193);
            this.boldOptionsDescriptionLabel4.Name = "boldOptionsDescriptionLabel4";
            this.boldOptionsDescriptionLabel4.Size = new System.Drawing.Size(75, 13);
            this.boldOptionsDescriptionLabel4.TabIndex = 5;
            this.boldOptionsDescriptionLabel4.Text = "Insert Data:";
            // 
            // optionsDescriptionLabel1
            // 
            this.optionsDescriptionLabel1.AutoSize = true;
            this.optionsDescriptionLabel1.Location = new System.Drawing.Point(144, 105);
            this.optionsDescriptionLabel1.MaximumSize = new System.Drawing.Size(300, 400);
            this.optionsDescriptionLabel1.Name = "optionsDescriptionLabel1";
            this.optionsDescriptionLabel1.Size = new System.Drawing.Size(174, 13);
            this.optionsDescriptionLabel1.TabIndex = 6;
            this.optionsDescriptionLabel1.Text = "Default server name is predefined.";
            // 
            // optionsDescriptionLabel4
            // 
            this.optionsDescriptionLabel4.AutoSize = true;
            this.optionsDescriptionLabel4.Location = new System.Drawing.Point(144, 193);
            this.optionsDescriptionLabel4.MaximumSize = new System.Drawing.Size(300, 400);
            this.optionsDescriptionLabel4.Name = "optionsDescriptionLabel4";
            this.optionsDescriptionLabel4.Size = new System.Drawing.Size(179, 13);
            this.optionsDescriptionLabel4.TabIndex = 7;
            this.optionsDescriptionLabel4.Text = "Populate the tables with initial data.";
            // 
            // optionsDescriptionLabel3
            // 
            this.optionsDescriptionLabel3.AutoSize = true;
            this.optionsDescriptionLabel3.Location = new System.Drawing.Point(144, 163);
            this.optionsDescriptionLabel3.MaximumSize = new System.Drawing.Size(300, 400);
            this.optionsDescriptionLabel3.Name = "optionsDescriptionLabel3";
            this.optionsDescriptionLabel3.Size = new System.Drawing.Size(265, 13);
            this.optionsDescriptionLabel3.TabIndex = 8;
            this.optionsDescriptionLabel3.Text = "Establish the new data structure within the database.";
            // 
            // optionsDescriptionLabel2
            // 
            this.optionsDescriptionLabel2.AutoSize = true;
            this.optionsDescriptionLabel2.Location = new System.Drawing.Point(144, 134);
            this.optionsDescriptionLabel2.MaximumSize = new System.Drawing.Size(300, 400);
            this.optionsDescriptionLabel2.Name = "optionsDescriptionLabel2";
            this.optionsDescriptionLabel2.Size = new System.Drawing.Size(271, 13);
            this.optionsDescriptionLabel2.TabIndex = 9;
            this.optionsDescriptionLabel2.Text = "Create a new database. Useful if you\'re starting fresh.";
            // 
            // serverNameLabel
            // 
            this.serverNameLabel.AutoSize = true;
            this.serverNameLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.serverNameLabel.Location = new System.Drawing.Point(55, 239);
            this.serverNameLabel.Name = "serverNameLabel";
            this.serverNameLabel.Size = new System.Drawing.Size(73, 13);
            this.serverNameLabel.TabIndex = 10;
            this.serverNameLabel.Text = "Server Name:";
            // 
            // serverNameTextEdit
            // 
            this.serverNameTextEdit.Location = new System.Drawing.Point(147, 236);
            this.serverNameTextEdit.Name = "serverNameTextEdit";
            this.serverNameTextEdit.Properties.NullText = "(localdb)\\MSSQLLocalDB";
            this.serverNameTextEdit.Size = new System.Drawing.Size(210, 20);
            this.serverNameTextEdit.TabIndex = 11;
            // 
            // createDatabaseCheckEdit
            // 
            this.createDatabaseCheckEdit.Location = new System.Drawing.Point(169, 282);
            this.createDatabaseCheckEdit.Name = "createDatabaseCheckEdit";
            this.createDatabaseCheckEdit.Properties.Caption = "Create Database";
            this.createDatabaseCheckEdit.Size = new System.Drawing.Size(136, 20);
            this.createDatabaseCheckEdit.TabIndex = 12;
            this.createDatabaseCheckEdit.CheckedChanged += new System.EventHandler(this.CreateDatabaseCheckEdit_CheckedChanged);
            // 
            // createSchemaCheckEdit
            // 
            this.createSchemaCheckEdit.Location = new System.Drawing.Point(169, 311);
            this.createSchemaCheckEdit.Name = "createSchemaCheckEdit";
            this.createSchemaCheckEdit.Properties.Caption = "Create Schema";
            this.createSchemaCheckEdit.Size = new System.Drawing.Size(136, 20);
            this.createSchemaCheckEdit.TabIndex = 13;
            this.createSchemaCheckEdit.CheckedChanged += new System.EventHandler(this.CreateSchemaCheckEdit_CheckedChanged);
            // 
            // insertDataCheckEdit
            // 
            this.insertDataCheckEdit.Location = new System.Drawing.Point(169, 340);
            this.insertDataCheckEdit.Name = "insertDataCheckEdit";
            this.insertDataCheckEdit.Properties.Caption = "Insert Data to Tables";
            this.insertDataCheckEdit.Size = new System.Drawing.Size(136, 20);
            this.insertDataCheckEdit.TabIndex = 14;
            // 
            // configureDatabaseButton
            // 
            this.configureDatabaseButton.Location = new System.Drawing.Point(169, 393);
            this.configureDatabaseButton.Name = "configureDatabaseButton";
            this.configureDatabaseButton.Size = new System.Drawing.Size(108, 23);
            this.configureDatabaseButton.TabIndex = 15;
            this.configureDatabaseButton.Text = "Configure";
            this.configureDatabaseButton.Click += new System.EventHandler(this.ConfigureDatabaseButton_Click);
            // 
            // ConfigureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 448);
            this.Controls.Add(this.configureDatabaseButton);
            this.Controls.Add(this.insertDataCheckEdit);
            this.Controls.Add(this.createSchemaCheckEdit);
            this.Controls.Add(this.createDatabaseCheckEdit);
            this.Controls.Add(this.serverNameTextEdit);
            this.Controls.Add(this.serverNameLabel);
            this.Controls.Add(this.optionsDescriptionLabel2);
            this.Controls.Add(this.optionsDescriptionLabel3);
            this.Controls.Add(this.optionsDescriptionLabel4);
            this.Controls.Add(this.optionsDescriptionLabel1);
            this.Controls.Add(this.boldOptionsDescriptionLabel4);
            this.Controls.Add(this.boldOptionsDescriptionLabel3);
            this.Controls.Add(this.boldOptionsDescriptionLabel2);
            this.Controls.Add(this.boldOptionsDescriptionLabel1);
            this.Controls.Add(this.configurationDBLabel);
            this.Controls.Add(this.databaseConfigLabel);
            this.MaximumSize = new System.Drawing.Size(460, 480);
            this.MinimumSize = new System.Drawing.Size(458, 478);
            this.Name = "ConfigureForm";
            this.Text = "ConfigureForm";
            ((System.ComponentModel.ISupportInitialize)(this.serverNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.createDatabaseCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.createSchemaCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.insertDataCheckEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label databaseConfigLabel;
        private System.Windows.Forms.Label configurationDBLabel;
        private System.Windows.Forms.Label boldOptionsDescriptionLabel1;
        private System.Windows.Forms.Label boldOptionsDescriptionLabel2;
        private System.Windows.Forms.Label boldOptionsDescriptionLabel3;
        private System.Windows.Forms.Label boldOptionsDescriptionLabel4;
        private System.Windows.Forms.Label optionsDescriptionLabel1;
        private System.Windows.Forms.Label optionsDescriptionLabel4;
        private System.Windows.Forms.Label optionsDescriptionLabel3;
        private System.Windows.Forms.Label optionsDescriptionLabel2;
        private System.Windows.Forms.Label serverNameLabel;
        private DevExpress.XtraEditors.TextEdit serverNameTextEdit;
        private DevExpress.XtraEditors.CheckEdit createDatabaseCheckEdit;
        private DevExpress.XtraEditors.CheckEdit createSchemaCheckEdit;
        private DevExpress.XtraEditors.CheckEdit insertDataCheckEdit;
        private DevExpress.XtraEditors.SimpleButton configureDatabaseButton;
    }
}