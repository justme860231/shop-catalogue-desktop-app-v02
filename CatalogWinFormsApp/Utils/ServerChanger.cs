using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Data.SqlClient;
using Dapper;

namespace CatalogWinFormsApp.Utils
{
    public class ServerChanger
    {
        private readonly string _filePath;

        public ServerChanger(string filePath)
        {
            _filePath = filePath;
        }

        public void SetServerName(string serverName)
        {
            string appsettingsJson = System.IO.File.ReadAllText(_filePath);

            dynamic jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(appsettingsJson);


            List<string> connectionStrings = new List<string> { "DefaultConnection", "StartingConnection" };
            if (jsonObj != null)
            {
                for (int i = 0; i < 2; i++)
                {
                    string connectionString = jsonObj["ConnectionStrings"][connectionStrings[i]].ToString();

                    List<string> connectionStringList = connectionString.Split(';').ToList<string>();
                    List<string> serverDefinitionList = connectionStringList[0].Split('=').ToList<string>();
                
                    serverDefinitionList[1] = serverName;

                    connectionStringList[0] = String.Join("=", serverDefinitionList.ToArray());
                
                    jsonObj["ConnectionStrings"][connectionStrings[i]] = String.Join(";", connectionStringList.ToArray());

                    File.WriteAllText("appsettings.json", "" + jsonObj);

                }
            }
        }

        public static string CheckSpecialChar(string name)
        {
            List<string> nameList = name.Select(c => c.ToString()).ToList();
            List<string> result = new List<string>();

            for (int i = 0; i < nameList.Count ; i++)
            {
                if (nameList[i] == "\\" && ((nameList[i - 1] != null && nameList[i - 1] != "\\")) | nameList[i - 1] == null)
                {
                    result.Add(nameList[i]);
                }
                result.Add(nameList[i]);
            }

            return String.Join("", result.ToArray());
        }

        public bool TestServerName(string serverName)
        {
            string connectionString = $"Server={serverName}; Trusted_Connection=true; TrustServerCertificate=true; Connection Timeout=5;";

            IDbConnection dbConnection = new SqlConnection(connectionString);
           
            bool result = false;

            try
            {
                DateTime outputTime = dbConnection.QuerySingle<DateTime>("SELECT GETDATE();");
                if (outputTime != null)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
                MessageBox.Show("Error: Connection with this Server Name is not possible to establish.", "Error");
            }

            return result;
        }
    }
}